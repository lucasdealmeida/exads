<?php
declare(strict_types=1);

namespace Tests\ASCII;

use App\ASCII\ASCII;
use PHPUnit\Framework\TestCase;

final class ASCIITest extends TestCase
{
    public function testGenerateRandom(): void
    {
        $range = ASCII::generateRandom(range(48, 52));
        $this->assertCount(5, $range);
        $this->assertContains('0', $range);
        $this->assertContains('1', $range);
        $this->assertContains('2', $range);
        $this->assertContains('3', $range);
        $this->assertContains('4', $range);

        $range2 = ASCII::generateRandom(range(65, 70));
        $this->assertCount(6, $range2);
        $this->assertContains('A', $range2);
        $this->assertContains('B', $range2);
        $this->assertContains('C', $range2);
        $this->assertContains('D', $range2);
        $this->assertContains('E', $range2);
        $this->assertContains('F', $range2);
    }

    public function testRemoveRandomly(): void
    {
        $range = ASCII::removeRandomly(['A', 'B', 'C']);
        $this->assertCount(2, $range);

        $range = ASCII::removeRandomly(['A', 'B', 'C', 'D', 'E']);
        $this->assertCount(4, $range);
    }

    public function testGetMissingCharacter(): void
    {
        $missingCharacter = ASCII::getMissingCharacter(['C', 'B', 'A'], ['A', 'C']);
        $this->assertEquals('B', $missingCharacter);

        $missingCharacter = ASCII::getMissingCharacter([5, 4, 1, 2, 3], [3, 5, 2, 4]);
        $this->assertEquals(1, $missingCharacter);
    }
}
