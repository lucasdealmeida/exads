<?php
declare(strict_types=1);

namespace Tests\PrimeNumbers;

use App\PrimeNumbers\Number;
use PHPUnit\Framework\TestCase;

final class NumberTest extends TestCase
{
    public function testPrimeNumber(): void
    {
        $number = new Number(1);
        $this->assertFalse($number->prime());

        $number = new Number(2);
        $this->assertTrue($number->prime());

        $number = new Number(4);
        $this->assertFalse($number->prime());
    }

    public function testPrimeNumberFactors(): void
    {
        $number = new Number(1);
        $this->assertEquals([1], $number->primeFactors());

        $number = new Number(2);
        $this->assertEquals([], $number->primeFactors());

        $number = new Number(4);
        $this->assertEquals([1,2,4], $number->primeFactors());
    }
}
