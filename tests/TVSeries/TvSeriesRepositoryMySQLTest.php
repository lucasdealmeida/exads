<?php
declare(strict_types=1);

namespace Tests\TVSeries;

use App\TVSeries\MySQLDatabase;
use App\TVSeries\TvSeriesRepositoryMySQL;
use DateTime;
use PHPUnit\Framework\TestCase;

final class TvSeriesRepositoryMySQLTest extends TestCase
{
    public function testSearchByDateAndTitle(): void
    {
        $baseSql = "
            select 
                * 
            from tv_series 
                left join tv_series_intervals on tv_series.id = tv_series_intervals.id_tv_series 
            where ";

        $where = ['tv_series_intervals.week_day = :week_day', 'tv_series_intervals.show_time = :show_time'];
        $where2 = [...$where, 'tv_series.title = :title'];

        $sql = $baseSql . implode(' and ', $where);
        $sql2 = $baseSql . implode(' and ', $where2);

        $params = [':week_day' => 'Monday', ':show_time' => '18:00:00'];
        $params2 = $params + [':title' => 'Some Title'];

        $mysqlDatabaseMock = $this->createMock(MySQLDatabase::class);
        $mysqlDatabaseMock
            ->expects($this->exactly(2))
            ->method('fetchAll')
            ->withConsecutive(
                [$this->equalTo($sql), $this->equalTo($params)],
                [$this->equalTo($sql2), $this->equalTo($params2)],
            );

        $dateTime = new DateTime();
        $dateTime->modify('next monday');
        $dateTime->setTime(18, 0);

        $tvSeriesRepositoryMysql = new TvSeriesRepositoryMySQL($mysqlDatabaseMock);
        $tvSeriesRepositoryMysql->searchByDateAndTitle($dateTime);
        $tvSeriesRepositoryMysql->searchByDateAndTitle($dateTime, 'Some Title');
    }
}
