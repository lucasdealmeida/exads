<?php
declare(strict_types=1);

namespace Tests\TVSeries;

use App\TVSeries\MySQLDatabase;
use PHPUnit\Framework\TestCase;

final class MySQLDatabaseTest extends TestCase
{
    public function testFetchAll(): void
    {
        $mysqlDatabase = new MySQLDatabase(
            host: '127.0.0.1',
            db: 'exads',
            username: 'root',
            password: 'root',
        );

        $mysqlDatabase->connect();
        $data = $mysqlDatabase->fetchAll('select * from tv_series where id=:id', [':id' => 1]);

        $this->assertEquals([
            [
                'id' => 1,
                'title' => 'PEACEMAKER',
                'channel' => 'HBO MAX',
                'gender' => 'Comedy',
            ],
        ], $data);
    }
}
