<?php
declare(strict_types=1);

namespace Tests\ASCII;

use App\ABTesting\ABTest;
use PHPUnit\Framework\TestCase;

final class ABTestTest extends TestCase
{
    public function testGetDesign(): void
    {
        $designs = [
            [
                'name' => 'Design 1',
                'splitPercent' => '75',
            ],
            [
                'name' => 'Design 2',
                'splitPercent' => '25',
            ],
        ];

        $design = ABTest::getDesign($designs);

        $this->assertArrayHasKey('name', $design);
        $this->assertArrayHasKey('splitPercent', $design);
    }
}
