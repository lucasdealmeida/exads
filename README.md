1 - install composer dependencies
==================

```
composer install
```



2 - starting a MariaDB instance on Docker
==================

```
docker run -d -p 3306:3306 --tmpfs /var/lib/mysql:rw --rm --name mysql_testing -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_USER=testing -e MYSQL_PASSWORD=testing -e MYSQL_DATABASE=testing mariadb:10.2
```




3 - run each index
==================


```php src/ABTesting/index.php```

```php src/ASCII/index.php```

```php src/PrimeNumbers/index.php```

```php src/TVSeries/index.php```



4 - run tests
==================


```vendor/bin/phpunit```   