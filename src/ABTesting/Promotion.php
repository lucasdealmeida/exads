<?php
declare(strict_types=1);

namespace App\ABTesting;

use Exads\ABTestData;
use Exads\ABTestException;

class Promotion
{
    /**
     * @param int $id
     * @return array
     * @throws ABTestException
     */
    public static function get(int $id): array
    {
        $abTest = new ABTestData($id);

        return [
            'id'      => $id,
            'name'    => $abTest->getPromotionName(),
            'designs' => $abTest->getAllDesigns()
        ];
    }
}