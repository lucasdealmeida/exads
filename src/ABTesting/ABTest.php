<?php
declare(strict_types=1);

namespace App\ABTesting;

class ABTest
{
    public static function getDesign($designs): array
    {
        $weights = array_map(fn($d) => $d['splitPercent'], $designs);
        $totalWeight = array_sum(array_column($designs, 'splitPercent'));

        $value = random_int(1, $totalWeight);
        foreach ($weights as $key => $weight) {
            $value -= $weight;

            if ($value <= 0) {
                return $designs[$key];
            }
        }

        return [];
    }
}