<?php

require __DIR__ . '/../../vendor/autoload.php';

foreach(range(1, 100) as $num) {
    $number = new App\PrimeNumbers\Number($num);

    $text = "PRIME";
    if ($number->prime() === false) {
        $text = implode(', ', $number->primeFactors());
    }

    echo "{$num} [$text] \r\n";
}
