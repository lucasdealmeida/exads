<?php
declare(strict_types=1);

namespace App\PrimeNumbers;

class Number
{
    public function __construct(
        public int $number,
    ) {}

    public function prime(): bool
    {
        if ($this->number == 1) {
            return false;
        }

        for ($i = 2; $i <= sqrt($this->number); $i++) {
            if ($this->number % $i == 0) {
                return false;
            }
        }

        return true;
    }

    public function primeFactors(): array
    {
        $multipleOf = [];
        for($count=2; $count < $this->number; $count++){
            if ($this->number % $count == 0) {
                $multipleOf[] = $count;
            }
        }

        if (count($multipleOf)) {
            $multipleOf[] = $this->number;
        }

        if ($this->number === 1 or count($multipleOf)) {
            array_unshift($multipleOf, 1);
        }

        return $multipleOf;
    }
}