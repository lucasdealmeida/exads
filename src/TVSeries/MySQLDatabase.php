<?php
declare(strict_types=1);

namespace App\TVSeries;

use PDO;

class MySQLDatabase extends Database
{
    public function connect(): void
    {
        $this->conn = new PDO("mysql:host={$this->host};dbname={$this->db}", $this->username, $this->password);
    }
}