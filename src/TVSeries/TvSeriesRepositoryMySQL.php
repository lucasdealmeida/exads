<?php
declare(strict_types=1);

namespace App\TVSeries;

use DateTime;

class TvSeriesRepositoryMySQL extends TvSeriesRepositoryContract
{
    public function searchByDateAndTitle(DateTime $dateTime, ?string $title = null): array
    {
        $params = [
            ':week_day' => $dateTime->format('l'),
            ':show_time' => $dateTime->format('H:i:s'),
        ];

        $where = [
            'tv_series_intervals.week_day = :week_day',
            'tv_series_intervals.show_time = :show_time',
        ];

        if ($title !== null) {
            $params[':title'] = $title;
            $where[] = 'tv_series.title = :title';
        }

        $sql = "
            select 
                * 
            from tv_series 
                left join tv_series_intervals on tv_series.id = tv_series_intervals.id_tv_series 
            where ". implode(' and ', $where)
        ;

        return $this->database->fetchAll($sql, $params);
    }
}