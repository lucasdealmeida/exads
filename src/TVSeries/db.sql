DROP TABLE IF EXISTS `tv_series`;
CREATE TABLE `tv_series` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(100) NOT NULL,
    `channel` varchar(100) NOT NULL,
    `gender` varchar(100) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `tv_series_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

INSERT INTO `tv_series` VALUES
    (1,'PEACEMAKER','HBO MAX','Comedy'),
    (2,'DEXTER','SHOWTIME','Drama'),
    (3,'BREAKING BAD','AMC','Crime'),
    (4,'GAME OF THRONES','HBO MAX','Action');

DROP TABLE IF EXISTS `tv_series_intervals`;
CREATE TABLE `tv_series_intervals` (
    `id_tv_series` int(11) NOT NULL,
    `week_day` varchar(20) NOT NULL,
    `show_time` time NOT NULL,
    KEY `tv_series_intervals_tv_series_id_fk` (`id_tv_series`),
    CONSTRAINT `tv_series_intervals_tv_series_id_fk` FOREIGN KEY (`id_tv_series`) REFERENCES `tv_series` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tv_series_intervals` VALUES
    (1,'monday','18:00:00'),
    (1,'friday','19:00:00'),
    (2,'wednesday','15:00:00'),
    (2,'sunday','16:00:00'),
    (3,'monday','18:00:00'),
    (3,'wednesday','15:00:00'),
    (4,'friday','19:00:00'),
    (4,'sunday','16:00:00');
