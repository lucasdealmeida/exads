<?php

require __DIR__ . '/../../vendor/autoload.php';

$mysqlDatabase = new \App\TVSeries\MySQLDatabase(
    host: "127.0.0.1",
    db: 'exads',
    username: 'root',
    password: 'root'
);
$mysqlDatabase->connect();

$dateTime = new DateTime();
$dateTime->modify('next monday');
$dateTime->setTime(18, 0);

$tvSeriesRepository = new \App\TVSeries\TvSeriesRepositoryMySQL($mysqlDatabase);
$tvSeriesOnMonday = $tvSeriesRepository->searchByDateAndTitle($dateTime);
$tvSeriesOnMonday2 = $tvSeriesRepository->searchByDateAndTitle($dateTime, 'PEACEMAKER');

print_r($tvSeriesOnMonday);
echo "\r\n\r\n";
print_r($tvSeriesOnMonday2);
echo "\r\n\r\n";
