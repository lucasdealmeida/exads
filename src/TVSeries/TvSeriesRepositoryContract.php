<?php
declare(strict_types=1);

namespace App\TVSeries;

use DateTime;

abstract class TvSeriesRepositoryContract
{
    public function __construct(
        protected Database $database
    ) { }

    abstract public function searchByDateAndTitle(DateTime $dateTime, ?string $title = null): array;
}