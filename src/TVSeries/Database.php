<?php
declare(strict_types=1);

namespace App\TVSeries;

use PDO;

abstract class Database
{
    protected PDO $conn;

    public function __construct(
        protected string $host,
        protected string $db,
        protected string $username,
        protected string $password,
    ) { }

    abstract public function connect(): void;

    public function fetchAll(string $query, array $params): array
    {
        $stmt = $this->conn->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}