<?php
declare(strict_types=1);

namespace App\ASCII;

class ASCII
{
    public static function generateRandom(array $range): array
    {
        $return = [];
        foreach($range as $ascii) {
            $return[] = chr($ascii);
        }

        shuffle($return);

        return $return;
    }

    public static function removeRandomly(array $data): array
    {
        unset($data[rand(0, count($data) - 1)]);

        return $data;
    }

    public static function getMissingCharacter(array $data1, array $data2): mixed
    {
        $arrayDiff = array_diff($data1, $data2);

        return reset($arrayDiff);
    }
}
