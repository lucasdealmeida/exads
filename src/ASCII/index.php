<?php

require __DIR__ . '/../../vendor/autoload.php';

$range = \App\ASCII\ASCII::generateRandom(range(44, 124));
$range2 = \App\ASCII\ASCII::removeRandomly($range);
$missingCharacter = \App\ASCII\ASCII::getMissingCharacter($range, $range2);

echo "Range: ";
echo "\r\n";
echo implode(' ', $range);

echo "\r\n\r\n";

echo "Range After Remove Randomly: ";
echo "\r\n";
echo implode(' ', $range2);

echo "\r\n\r\n";

echo "Missing Character: {$missingCharacter} \r\n";